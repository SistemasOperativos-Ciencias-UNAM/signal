/* https://gist.github.com/aspyct/3462238 */

/**
 * More info?
 * a.dotreppe@aspyct.org
 * http://aspyct.org
 * @aspyct (twitter)
 *
 * Hope it helps :)
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h> /* sigaction(), sigsuspend(), sig*() */
#include <unistd.h> /* sleep(); */

int loop_sleep = 1;
int signal_sleep = 10;

void handle_signal(int signal);

/* Usage example
 *
 * First, compile and run this program:
 *     $ gcc signal.c
 *     $ ./a.out
 *
 * It will print out its pid. Use it from another terminal to send signals
 *     $ kill -HUP <pid>
 *     $ kill -USR1 <pid>
 *     $ kill -USR2 <pid>
 *
 * Exit the process with ^C ( = SIGINT) or SIGKILL, SIGTERM
 */
int main()
{
    struct sigaction sa;

    /* Print pid, so that we can send signals from other shells */
    printf("My pid is: %d\n", getpid());

    /* Setup the sighub handler */
    sa.sa_handler = &handle_signal;

    /* Restart the system call, if at all possible */
    sa.sa_flags = SA_RESTART;

    /* Block every signal during the handler */
    sigfillset(&sa.sa_mask);

    /* Will always fail, SIGKILL is intended to force kill your process */
    if (sigaction(SIGKILL, &sa, NULL) == -1) {
        perror("Cannot handle SIGKILL"); /* Will always happen */
        printf("You can never handle SIGKILL anyway...\n");
    }

    /* Intercept SIGHUP, SIGINT, SIGPIPE, SIGALRM, SIGTERM, SIGUSR1, SIGUSR2 and SIGCHLD */
    if (sigaction(SIGHUP, &sa, NULL) == -1)
        perror("Error: cannot handle SIGHUP"); /* Should not happen */

    if (sigaction(SIGINT, &sa, NULL) == -1)
        perror("Error: cannot handle SIGINT"); /* Should not happen */

    if (sigaction(SIGTERM, &sa, NULL) == -1)
        perror("Error: cannot handle SIGTERM"); /* Should not happen */

    if (sigaction(SIGPIPE, &sa, NULL) == -1)
        perror("Error: cannot handle SIGPIPE"); /* Should not happen */

    if (sigaction(SIGALRM, &sa, NULL) == -1)
        perror("Error: cannot handle SIGALRM"); /* Should not happen */

    if (sigaction(SIGTERM, &sa, NULL) == -1)
        perror("Error: cannot handle SIGTERM"); /* Should not happen */

    if (sigaction(SIGUSR1, &sa, NULL) == -1)
        perror("Error: cannot handle SIGUSR1"); /* Should not happen */

    if (sigaction(SIGUSR2, &sa, NULL) == -1)
        perror("Error: cannot handle SIGUSR2"); /* Should not happen */

    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        perror("Error: cannot handle SIGCHLD"); /* Should not happen */

    for (;;) {
        printf("\nSleeping for %d seconds\n",loop_sleep);
        sleep(loop_sleep);
    }
}

void handle_signal(int signal)
{
    const char *signal_name;
    sigset_t pending;

    /* Find out which signal we're handling */
    switch (signal) {
        case SIGHUP:
            signal_name = "SIGHUP";
            break;
        case SIGINT:
            printf("Caught SIGINT, exiting now\n");
            exit(0);
        case SIGPIPE:
            signal_name = "SIGPIPE";
            break;
        case SIGALRM:
            signal_name = "SIGALRM";
            break;
        case SIGTERM:
            signal_name = "SIGTERM";
            printf("My pid was: %d\n", getpid());
            break;
        case SIGUSR1:
            signal_name = "SIGUSR1";
            break;
        case SIGUSR2:
            signal_name = "SIGUSR2";
            break;
        case SIGCHLD:
            signal_name = "SIGCHLD";
            break;
        default:
            fprintf(stderr, "Caught wrong signal: %d\n", signal);
            return;
    }

    /*
     * Please note that printf et al. are NOT safe to use in signal handlers.
     * Look for async safe functions.
     */
    printf("Caught %s, sleeping for %d seconds\n"
           "Try sending another SIGHUP / SIGINT / SIGUSR1"
           "(or more) meanwhile\n", signal_name, signal_sleep);
    /*
     * Indeed, all signals are blocked during this handler
     * But, at least on OSX, if you send 2 other SIGHUP,
     * only one will be delivered: signals are not queued
     * However, if you send HUP, INT, HUP,
     * you'll see that both INT and HUP are queued
     * Even more, on my system, HUP has priority over INT
     */
    sleep(3);
    printf("Done sleeping for %s\n", signal_name);

    /* So what did you send me while I was asleep? */
    sigpending(&pending);
    if (sigismember(&pending, SIGHUP))
        printf("A SIGHUP is waiting\n");
    if (sigismember(&pending, SIGUSR1))
        printf("A SIGUSR1 is waiting\n");
    if (sigismember(&pending, SIGUSR2))
        printf("A SIGUSR2 is waiting\n");

    printf("Done handling %s\n\n", signal_name);
}
