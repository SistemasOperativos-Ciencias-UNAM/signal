/* http://www.thegeekstuff.com/2011/02/send-signal-to-process/ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc,char *argv[])
{
  int ret,pid,sig;
  if (argc < 2)
  {
    printf("Usage:\n");
    printf("%s  <pid> <signum> \n",argv[0]);
    printf("\nList of signals with `kill -l`\n");
    return(1);
  }

  pid = atoi(argv[1]);
  sig = atoi(argv[2]);
  ret = kill(pid,sig);
  printf("ret : %d\n",ret);
  return(ret);
}
